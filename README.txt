virtuoso-foobar is a repository for testing and experimenting Virtuoso + Python solutions.

It was planned to be used inside a docker container [1] that accesses a Virtuoso database in another docker container[2]

When deploying this repo, the virtodbcu_r.so file must be copied to /usr/local/virtuoso-opensourse, and the odbc.ini file must be copied to /etc

[1] https://hub.docker.com/r/jtmolon/vos-demo-client/ - docker pull jtmolon/vos-demo-client
[2] https://hub.docker.com/r/jtmolon/vos-demo-server/ - docker pull jtmolon/vos-demo-server



---------------------------------------------------------
TESTING


# setting up server container
docker pull jtmolon/vos-demo-server
# without mapping virtuoso ports to local ports
# through this method, the server can be accessed from the browser using:
# 1 - localhost:mapped_port/sparql
#     the mapped port can be seen with docker ps
#
# 2 - container_ip:8890/sparql
#     the container ip can be obtained with the command docker inspect --format '{{ .NetworkSettings.IPAddress }}' id_container
#
docker run --name test_server jtmolon/vos-demo-server -P -d bin/sh -c "/opt/virtuoso-opensource/bin/virtuoso-t -f"
# mapping virtuoso ports to local ports
# through this method, the server can be accessed from the browser with localhost:8890/sparql
docker run --name test_server jtmolon/vos-demo-server -p 1111:1111 -p 8890:8890 -d bin/sh -c "/opt/virtuoso-opensource/bin/virtuoso-t -f"


# setting up client container
docker pull jtmolon/vos-demo-client
docker run --name test_client -it -d jtmolon/vos-demo-client
# to access the client shell
docker exec -it test_client /bin/bash

from datetime import datetime

from rdflib import Graph
from rdflib import Namespace
from rdflib.namespace import RDF, FOAF
from rdflib import URIRef, BNode, Literal
from rdflib.store import Store
from rdflib.plugin import get as plugin

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from virtuoso.vmapping import (
    QuadMapPattern, QuadStorage, GraphQuadMapPattern, IriClass,
        PatternGraphQuadMapPattern, ClassPatternExtractor, VirtRDF)
from virtuoso.vstore import Virtuoso
from virtuoso.alchemy import SparqlClause

from models import CollaborationRegister
from models import CommunityPractice


NSPC = Namespace("http://example.org/people/")
bob = NSPC.bob
linda = NSPC.linda
name = Literal('Bob') # passing a string
age = Literal(24) # passing a python int
height = Literal(76.5) # passing a python float


def setup_sqlalchemy():
    """Sets graph and store with SQLAlchemy"""
    admin_engine = create_engine('virtuoso://dba:dba@VOS')
    SessionMaker = sessionmaker(admin_engine)
    session = SessionMaker()
    store = Virtuoso(connection=session.bind.connect())
    g = Graph(store=store, identifier = URIRef(default_graph_uri))
    return g, store

def setup_rdflib_plugin():
    VirtuosoPlugin = plugin("Virtuoso", Store)
    store = VirtuosoPlugin("DSN=VOS;UID=dba;PWD=dba;WideAsUTF16=Y")
    default_graph_uri = "http://exemple.org/dio_graph"
    g = Graph(store=store, identifier = URIRef(default_graph_uri))
    return g, store

def add_cop_test(g, store):
    CommunityPractice.db = g
    cop_uid = URIRef(NSPC.developers)
    cop = CommunityPractice(cop_uid)
    cop.community_id = cop_uid
    cop.name = Literal(u"Developers")
    cop.creator_id = bob
    cop.creation_date = Literal(datetime.now())

def add_bob(g, store):
    g.add( (bob, RDF.type, FOAF.Person) )
    g.add( (bob, FOAF.name, name) )
    g.add( (bob, FOAF.knows, linda) )
    g.add( (linda, RDF.type, FOAF.Person) )
    g.add( (linda, FOAF.name, Literal('Linda') ) )
    store.commit()
    g.close()


if __name__ == '__main__':
    import ipdb; ipdb.set_trace()
    g, store = setup_rdflib_plugin()
    add_cop_test(g, store)

# -*- coding: utf-8
from rdfalchemy import rdfSubject
from rdfalchemy import rdfSingle
from rdflib import Namespace

COPSEMANTIC = Namespace("http://localhost:8080/copsemantic/rdfs-copsemantic#")
COPONTOLOGY = Namespace("http://localhost:8080/copsemantic/owl-copontology#")
SIOC = Namespace("http://rdfs.org/sioc/ns#")

class CollaborationRegister(rdfSubject):
    """Classe do registro de colaboracao
        copsemantic:id_comunidade
        copsemantic:id_criador
        copsemantic:tipo_conteudo
        copsemantic:id_conteudo
        copsemantic:data_criacao
    """
    rdf_type = COPSEMANTIC.CollaborationRegister
    community_id = rdfSingle(COPSEMANTIC.community_id, 'community_id')
    creator_id = rdfSingle(COPSEMANTIC.creator_id, 'creator_id')
    content_type = rdfSingle(COPSEMANTIC.content_type, 'content_type')
    content_id = rdfSingle(COPSEMANTIC.content_id, 'content_id')
    creation_date = rdfSingle(COPSEMANTIC.creation_date, 'creation_date')
    register_type = rdfSingle(COPSEMANTIC.register_type, 'register_type')
    cop_context = rdfSingle(COPSEMANTIC.cop_context, 'cop_context')
    domain = rdfSingle(COPSEMANTIC.domain, 'domain')


class CommunityPractice(rdfSubject):
    """Classe da comunidade de pratica
    """
    rdf_type = COPSEMANTIC.CommunityOfPractice
    community_id = rdfSingle(COPSEMANTIC.community_id, 'community_id')
    name = rdfSingle(SIOC.title, 'title')
    creator_id = rdfSingle(COPSEMANTIC.creator_id, 'creator_id')
    creation_date = rdfSingle(COPSEMANTIC.creation_date, 'creation_date')
    domain = rdfSingle(COPSEMANTIC.domain, 'domain')
